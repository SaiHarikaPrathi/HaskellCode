data Choice
  = Rock
  | Paper
  | Scissors
  deriving (Show, Eq)
 
beats :: Choice -> Choice -> Bool
beats Paper Rock = True
beats Scissors Paper = True
beats Rock Scissors = True
beats _ _ = False

game p1 p2 = if beats p1 p2 then "player wins\n" else if beats p2 p1 then "player loses\n" else "draw\n"

main = do

putStrLn("Enter Rock : 1, Paper : 2, Scissors : 3")
putStrLn("Player 1 :")
player1 <- rps <$> getLine
  where
    rps "scissors" = Scissors
    rps "rock" = Rock
    rps "paper" = Paper
    rps _ = error "invalid input"
putStrLn("Player 2 :")
player2 <- rps <$> getLine
  where
    rps "scissors" = Scissors
    rps "rock" = Rock
    rps "paper" = Paper
    rps _ = error "invalid input"
    
putStrLn(game player1 player2)